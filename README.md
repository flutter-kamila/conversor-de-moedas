# Conversor de Moedas

Aplicativo Flutter que converte moedas (reais, euro, dolar) consumindo Api via requisição http.

- Versão atualizada do plugin http: https://pub.dartlang.org/packages/http#-installing-tab-

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

- Visual Studio Code:
    - Linha de comando: `flutter doctor`.
    - Instalar extensões: Flutter, Dart
    - Ctrl + Shift + P, click "Flutter: New Project": criar projeto
    - Ctrl + F5: rodar do smartphone
    - Shift + Alt + F: identação

### Developer

[Kamila Serpa](https://kamilaserpa.github.io)