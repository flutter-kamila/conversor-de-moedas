/**
 * Desenvolvedora: Kamila Serpa, https://kamilaserpa@github.io
 */

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

const request = "https://api.hgbrasil.com/finance?format=json&key=bc7a6243";

void main() async {
  runApp(MaterialApp(
        home: Home(),
        theme: ThemeData(
            hintColor: Colors.amber,
            primaryColor: Colors.white,
            inputDecorationTheme: InputDecorationTheme(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.amber)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.amber)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.amber)),
            ))),
  );
}

// REQUISIÇÃO
Future<Map> getData() async {
  http.Response response = await http.get(request);
  return json.decode(response.body);
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final realController = TextEditingController();
  final dolarController = TextEditingController();
  final euroController = TextEditingController();

  // Valor das cotações das moedas recebido por API
  double dolar;
  double euro;

  void _realChanged(String text) {
    if (text.isEmpty) {
      _clearAll();
      return;
    }
    double inputReal = double.parse(text);
    dolarController.text = (inputReal / dolar).toStringAsFixed(2); // Duas casas decimais
    euroController.text = (inputReal / euro).toStringAsFixed(2);
  }

  void _dolarChanged(String text) {
    if (text.isEmpty) {
      _clearAll();
      return;
    }
    double inputDolar = double.parse(text);
    realController.text = (inputDolar * this.dolar).toStringAsFixed(2); // converte p/ reais
    euroController.text = (inputDolar * this.dolar / euro).toStringAsFixed(2);
  }

  void _euroChanged(String text) {
    if (text.isEmpty) {
      _clearAll();
      return;
    }
    double inputEuro = double.parse(text);
    realController.text =
        (inputEuro * this.euro).toStringAsFixed(2); // converte p/ reais
    dolarController.text = (inputEuro * this.euro / dolar).toStringAsFixed(2);
  }

  void _clearAll() {
    realController.text = "";
    dolarController.text = "";
    euroController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //Widget que fornece Barra Superior Header
        backgroundColor: Colors.black,
        // HEADER
        appBar: AppBar(
            title: Text("\$ Conversor \$"),
            backgroundColor: Colors.amber,
            centerTitle: true),
        // BODY
        body: FutureBuilder<Map>(
          future: getData(), // REQUISIÇÃO / REQUEST
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return Center(
                    child: Text("Carregando dados",
                        style: TextStyle(color: Colors.amber, fontSize: 24.0),
                        textAlign: TextAlign.center));
              default:
                if (snapshot.hasError) {
                  // ERROR
                  return Center(
                      child: Text(
                    "Erro ao carregar os dados",
                    style: TextStyle(color: Colors.amber, fontSize: 24.0),
                    textAlign: TextAlign.center,
                  ));
                } else {
                  // SUCCESS
                  dolar = snapshot.data["results"]["currencies"]["USD"]["buy"];
                  euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];

                  return SingleChildScrollView(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment:
                          CrossAxisAlignment.stretch, // Alargando coluna
                      children: <Widget>[
                        // iCONE
                        Icon(Icons.monetization_on,
                            size: 150.0, color: Colors.amber),

                        // INPUT REAIS
                        buildTextField(
                            "Reais", "R\$ ", realController, _realChanged),
                        Divider(), //Espaçamento vertical
                        // INPUT DOLAR
                        buildTextField(
                            "Dólares", "US\$ ", dolarController, _dolarChanged),
                        Divider(), //Espaçamento vertical
                        // INPUT EURO
                        buildTextField(
                            "Euros", "€ ", euroController, _euroChanged),
                      ],
                    ),
                  );
                }
            }
          },
        ));
  }
}

Widget buildTextField(String label, String prefixo, TextEditingController c, Function f) {
  return TextField(
    controller: c, // Bind
    decoration: InputDecoration(
      labelText: label,
      labelStyle: TextStyle(color: Colors.amber),
      border: OutlineInputBorder(),
      prefixText: prefixo,
    ),
    style: TextStyle(
        // Cor do texto input
        color: Colors.amber,
        fontSize: 24.0),
    onChanged: f,
    keyboardType: TextInputType.numberWithOptions(decimal: true), // Para digitar decimais no IOS
  );
}
